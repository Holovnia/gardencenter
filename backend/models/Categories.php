<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property string $description
 * @property string $category_url
 * @property integer $is_active
 * @property string $category_image
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'category_url', 'category_image'], 'required'],
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['category_name', 'category_url', 'category_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'ID категорії',
            'category_name' => 'Назва категорії',
            'description' => 'Description',
            'category_url' => 'Url категорії',
            'is_active' => 'Is Active',
            'category_image' => 'Зображення',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }
}
