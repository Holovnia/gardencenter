<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `backend\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id', 'status_id', 'is_active'], 'integer'],
            [['product_name_lat', 'product_name_ua', 'description', 'pot', 'size', 'shape', 'image', 'meta_description', 'meta_keywords'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_id' => $this->product_id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'status_id' => $this->status_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'product_name_lat', $this->product_name_lat])
            ->andFilterWhere(['like', 'product_name_ua', $this->product_name_ua])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'pot', $this->pot])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'shape', $this->shape])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords]);

        return $dataProvider;
    }
}
