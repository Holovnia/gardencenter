<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $product_id
 * @property string $product_name_lat
 * @property string $product_name_ua
 * @property string $description
 * @property integer $category_id
 * @property string $pot
 * @property string $size
 * @property string $shape
 * @property string $price
 * @property integer $status_id
 * @property integer $is_active
 * @property string $image
 * @property string $meta_description
 * @property string $meta_keywords
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'meta_description', 'meta_keywords'], 'required'],
            [['category_id', 'status_id', 'is_active'], 'integer'],
            [['price'], 'number'],
            [['product_name_lat', 'product_name_ua', 'description', 'pot', 'size', 'shape', 'image', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_name_lat' => 'Product Name Lat',
            'product_name_ua' => 'Product Name Ua',
            'description' => 'Description',
            'category_id' => 'Category ID',
            'pot' => 'Pot',
            'size' => 'Size',
            'shape' => 'Shape',
            'price' => 'Price',
            'status_id' => 'Status ID',
            'is_active' => 'Is Active',
            'image' => 'Image',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }
}
