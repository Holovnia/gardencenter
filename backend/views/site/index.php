<?php

/* @var $this yii\web\View */

$this->title = 'Админка Садовий центр';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Адмінистративна панель</h1>

        <p class="lead">Ви маєте можливість створювати, редагувати і видаляти категорії та товари.</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Категорії</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://gardencenter/backend/web/index.php?r=categories%2Findex">Категорії &raquo;</a></p>
            </div>
            <div class="col-lg-6">
                <h2>Товари</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://gardencenter/backend/web/index.php?r=products%2Findex">Товари &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
