<?php

/* @var $this yii\web\View */

$this->title = 'Садовий центр';
?>

<div class="site-index">
    <div class="jumbotron">
        <h1>Послуги садового центру</h1>
        <p class="lead">Якість обумовлена досвідом</p>
    </div>
    <!--content -->
    <div class="body-content">
        <div class="row">
            <div class="col-lg-6">
                <h2>Вирощування</h2>
                <img src="../views/img/icon-growing.png" alt="growing">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
                <p><a class="btn btn-default" href="#">Вирощування &raquo;</a></p>
            </div>
            <div class="col-lg-6">
                <h2>Догляд</h2>
                <img src="../views/img/icon-care.png" alt="care">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
                <p><a class="btn btn-default" href="#">Догляд &raquo;</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <h2>Озелення</h2>
                <img src="../views/img/icon-landscaping.png" alt="landscaping">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="#">Озелення &raquo;</a></p>
            </div>
            <div class="col-lg-6">
                <h2>Торгівля</h2>
                <img src="../views/img/icon-trade.png" alt="trade">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Каталог &raquo;</a></p>
            </div>
        </div>
    </div>
    <!-- end content -->
</div>
