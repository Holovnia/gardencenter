<?php

use yii\helpers\Html;

$this->title = 'Каталог товарів';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-catalog">
    <h1><?= Html::encode($this->title) ?></h1>

    <h3>Категорії</h3>

    <?php foreach ( $catalog  as $catalogy): ?>
        <li>
            <?= "<img src='$catalogy->category_image'>" . " " . "<h4 style='display:inline-table'>$catalogy->category_name</h4>"; ?>
            <?php//echo $catalogy->description ?>
        </li>
    <?php endforeach; ?>
    <?php
        $session = Yii::$app->session;
        $language = $session['language'];
        echo $language;
    ?>

</div>
